{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Main where

import           Control.Applicative ((<$>))
import           Control.Exception
import           Control.Lens hiding ((|>))
import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.Reader (ask)
import           Control.Monad.State (get, put)
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Control.Monad.Trans.Resource
import qualified Data.ByteString.Char8 as B8
import qualified Data.Conduit as C
import qualified Data.Conduit.List as CL
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           Data.Typeable (Typeable)
import           Network.HTTP.Conduit
import           System.Environment (getEnv)
import           System.IO (hFlush, readFile, stdout, writeFile)
import qualified Web.Authenticate.OAuth as OA
import           Web.Twitter.Conduit
import           Web.Twitter.Types.Lens

type TweetText = T.Text
type Username  = T.Text

data ReducedStatus = ReducedStatus {
    rdsText     :: TweetText
  , rdsUserName :: Username
    -- type LanguageCode = String
  , rdsLang     :: Maybe LanguageCode
    -- Integer
  , rdsStatusId :: StatusId
  } deriving (Show, Typeable)

authorize :: (MonadBaseControl IO m, MonadResource m)
          => OA.OAuth -- ^ OAuth Consumer key and secret
          -> (String -> m String) -- ^ PIN prompt
          -> Manager
          -> m OA.Credential
authorize oauth getPIN mgr = do
    cred <- OA.getTemporaryCredential oauth mgr
    let url = OA.authorizeUrl oauth cred
    pin <- getPIN url
    OA.getAccessToken oauth
      (OA.insert "oauth_verifier" (B8.pack pin) cred) mgr

getTWInfo :: IO TWInfo
getTWInfo = do
  key <- getEnv "OAUTH_KEY"
  secret <- getEnv "OAUTH_SECRET"
  let tokens = twitterOAuth {
          OA.oauthConsumerKey = B8.pack key
        , OA.oauthConsumerSecret = B8.pack secret
        }
  mgr <- newManager tlsManagerSettings
  cred <- runResourceT $ authorize tokens getPIN mgr
  return $ setCredential tokens cred OA.def
  where
    getPIN url = liftIO $ do
        putStrLn $ "browse URL: " ++ url
        putStr "> what was the PIN twitter provided you with? "
        hFlush stdout
        getLine

printStatus :: Status -> IO ()
printStatus status = TIO.putStrLn texty
  where texty = T.concat [ T.pack . show $ status ^. statusId
                         , ": "
                         , status ^. statusUser . userScreenName
                         , ": "
                         , status ^. statusText
                         ]

toReducedStatus :: Status -> ReducedStatus
toReducedStatus status = rds
  where rds = ReducedStatus txt user lang sid
        txt  = status ^. statusText
        user = status ^. statusUser . userScreenName
        lang = status ^. statusLang
        sid  = status ^. statusId

foldStream :: TWInfo
           -> Int
           -> UserParam
           -> IO ()
foldStream twInfo numTweets user = do
  mgr <- newManager tlsManagerSettings
  runResourceT $ sourceWithMaxId twInfo mgr (userTimeline user)
    C.$= CL.isolate numTweets
    C.$$ CL.mapM_ (\t -> liftIO (print t))

killFavs :: TWInfo
         -> IO ()
killFavs twInfo = do
  mgr <- newManager tlsManagerSettings
  runResourceT $ sourceWithMaxId twInfo mgr (favoritesList Nothing)
    -- C.$= CL.isolate 1
    C.$$ CL.mapM_ (\t -> void $ call twInfo mgr $ favoritesDestroy (t ^. statusId))

saveCredentials :: TWInfo -> IO ()
saveCredentials twinfo = writeFile ".credentials" (show twinfo)

loadCredentials :: IO (Maybe TWInfo)
loadCredentials = do
  tw <- readFile ".credentials"
  return $ Just (read tw)

withCredentials action = do
  twinfo <- loadCredentials `catch` handleMissing
  case twinfo of
    Nothing     -> getTWInfo >>= saveCredentials
    Just twinfo -> action twinfo
  where handleMissing :: IOException -> IO (Maybe TWInfo)
        handleMissing _ = return Nothing

main :: IO ()
main = do
  withCredentials killFavs
    -- twInfo <- getTWInfo
    -- m <- foldStream twInfo 100 (ScreenNameParam "argumatronic")
    -- killFavs twInfo
